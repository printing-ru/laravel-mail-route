<?php
/**
 * kernel.
 * Date: 08/05/17
 * Time: 11:02
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMailRoute;

use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\View;
use InvalidArgumentException;

class ViewMailer implements Mailer
{
    protected $html;
    protected $text;
    protected $raw;

    public function make(Mailable $mailable) {
        $mailable->send($this);
        return $this;
    }

    /**
     * @param $format
     * @return mixed
     */
    public function format($format) {
        if (!in_array($format, ['html', 'text', 'raw'])) {
            $format = 'html';
        }
        return $this->$format;
    }

    /**
     * Send a new message when only a raw text part.
     *
     * @param  string $text
     * @param  mixed $callback
     * @return void
     */
    public function raw($text, $callback)
    {
        $this->send(['raw' => $text], [], $callback);
    }


    /**
     * Send a new message using a view.
     *
     * @param  string|array  $view
     * @param  array  $data
     * @param  \Closure|string  $callback
     * @return void
     */
    public function send($view, array $data = [], $callback = null)
    {
        list($view, $plain, $raw) = $this->parseView($view);
        $this->addContent($view, $plain, $raw, $data);
    }

    /**
     *
     * @param  string  $view
     * @param  string  $plain
     * @param  string  $raw
     * @param  array  $data
     *
     * @return void
     */
    protected function addContent($view, $plain, $raw, $data)
    {
        if (isset($view)) {
            $this->html = $this->renderView($view, $data);
        }

        if (isset($plain)) {
            $this->text = $this->renderView($plain, $data);
        }

        if (isset($raw)) {
            $this->raw = $raw;
        }
    }

    /**
     * Render the given view.
     *
     * @param  string  $view
     * @param  array  $data
     * @return string
     */
    protected function renderView($view, $data)
    {
        return $view instanceof Htmlable
            ? $view->toHtml()
            : View::make($view, $data);
    }

    /**
     * Get the array of failed recipients.
     *
     * @return array
     */
    public function failures()
    {
        return [];
    }

    /**
     * Parse the given view name or array.
     *
     * @param  string|array  $view
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    protected function parseView($view)
    {
        if (is_string($view)) {
            return [$view, null, null];
        }

        // If the given view is an array with numeric keys, we will just assume that
        // both a "pretty" and "plain" view were provided, so we will return this
        // array as is, since must should contain both views with numeric keys.
        if (is_array($view) && isset($view[0])) {
            return [$view[0], $view[1], null];
        }

        // If this view is an array but doesn't contain numeric keys, we will assume
        // the views are being explicitly specified and will extract them via the
        // named keys instead, allowing the developers to use one or the other.
        if (is_array($view)) {
            return [
                Arr::get($view, 'html'),
                Arr::get($view, 'text'),
                Arr::get($view, 'raw'),
            ];
        }

        throw new InvalidArgumentException('Invalid view.');
    }
}
