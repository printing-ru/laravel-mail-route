<?php
/**
 * laravel-mail-route.
 * Date: 08/05/17
 * Time: 15:39
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMailRoute;


use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MailRoute
{
    /**
     * Register all routes/mail.php routes
     *
     * @param string $prefix
     * @param string $middleware
     */
    public static function routes($prefix = 'mail', $middleware = 'web') {
        Route::prefix($prefix)
            ->middleware($middleware)
            ->group(sprintf('%s/routes/mail.php', App::basePath()));
    }

    /**
     * @param $action
     * @return \Closure
     */
    protected static function mailRouteWrapper($action) {
        return function (Request $request, $email = FALSE) use($action) {
            $result = App::call($action);
            if ($result instanceof Mailable) {
                if ($request->has('lang')) {
                    App::setLocale($request->get('lang'));
                }
                //send email if it is presented
                if (FALSE !== $email && FALSE !== $email = filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    //clear all receipients
                    $result->to = $result->cc = $result->bcc = [];
                    Mail::to($email)->send($result);
                    return 'Email was sent to ' . $email;
                }
                return (new ViewMailer)->make($result)->format($request->get('format', 'html'));
            }
            return $result;
        };
    }
    /**
     * Register a new GET route with the router for the mail.
     *
     * @param $uri
     * @param null $action
     * @return mixed
     */
    public static function mail($uri, $action = null) {
        return Route::get(sprintf('%s/{email?}', $uri), self::mailRouteWrapper($action));
    }
}