<?php
namespace Tests;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use \Mockery as m;
use NavinLab\LaravelMailRoute\MailRoute;
use PHPUnit\Framework\TestCase;

class MailRouteTest extends TestCase
{
    public function testRoutes() {
        App::shouldReceive('basePath')->andReturn('');

        Route::shouldReceive('prefix')->with('prefix')->andReturnSelf();
        Route::shouldReceive('middleware')->with('test')->andReturnSelf();
        Route::shouldReceive('group')->with('/routes/mail.php')->andReturnSelf();

        MailRoute::routes('prefix', 'test');

        $this->assertTrue(true);
    }

    public function testMail() {
        Route::shouldReceive('get')->once()->andReturn(null);

        $action = function () {};
        $route = MailRoute::mail('test', $action);

        $this->assertNULL($route);
    }
}