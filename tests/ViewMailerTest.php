<?php
namespace Tests;

use Illuminate\Support\Facades\View;
use \Mockery as m;
use NavinLab\LaravelMailRoute\ViewMailer;
use PHPUnit\Framework\TestCase;

class ViewMailerTest extends TestCase
{
    /**
     * @inheritdoc
     */
    protected function setUp() {
        parent::setUp();
    }

    public function testDontRunCallback() {
        $isNotRun = true;
        $action = function () use(&$isNotRun) {
            $isNotRun = false;
        };

        View::shouldReceive('make')
            ->once()
            ->andReturn('text');

        $viewMailer = new ViewMailer;
        $viewMailer->raw('test', $action);
        $viewMailer->send('test', [], $action);

        $this->assertTrue($isNotRun);
    }

    /**
     *
     */
    public function testRaw() {
        $viewMailer = new ViewMailer;
        $viewMailer->raw('test', function () {});
        $this->assertEquals('test', $viewMailer->format('raw'));
    }

    /**
     * Action test
     */
    public function testAvailableFormats()
    {
        View::shouldReceive('make')
            ->twice()
            ->andReturn('html', 'text');

        $viewMailer = new ViewMailer;
        $viewMailer->send(['html' => 'html', 'text' => 'text', 'raw' => 'raw']);

        $this->assertEquals('html', $viewMailer->format('html'));
        $this->assertEquals('text', $viewMailer->format('text'));
        $this->assertEquals('raw', $viewMailer->format('raw'));
        $this->assertEquals('html', $viewMailer->format('unknown'));
    }

}