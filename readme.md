# Laravel Mail Routes


### Instalation
```sh
    composer require navinlab/laravel-mail-route
```

### Configuration
Register mail routes in your **RouteServiceProvider.php**

```php
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        
        //to enable only for the local development
        if (App::isLocal()) {
            MailRoute::routes();
        }
    }
```
It will create mail route group with prefix 'mail' and attache all routes/mail.php.

Create **routes/mail.php** file.

### Usage
Add you first mail route in **routes/mail.php**
Use **MailRoute::mail** static method to wrap your mailable object action

```php
    use App\Mail\Auth\ResetPassword;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Route;
    use NavinLab\LaravelMailRoute\MailRoute;
    
    Route::group(['prefix' => 'auth'], function () {
    
        //reset password email
        MailRoute::mail('password/forgot', function (Request $request) {
            return new ResetPassword($request->get('token', 'token'));
        });
    });
```
ResetPassword implements the Mailable interface. 
You can use artisan command to generate it.

```sh
    php artisan make::mail Auth/ResetPassword --markdown=reset.password 
```

Now you can request your mail template
```
    http://app.localhost/mail/auth/password/forgot 
``` 
### Formating
if you Mailable implementation supports text or raw formats, you can check it also, just add format parameter to mail request.
```
    http://app.localhost/mail/auth/password/forgot?format=text
    http://app.localhost/mail/auth/password/forgot?format=raw
    
    http://app.localhost/mail/auth/password/forgot?format=html
```
Format **html** is default format.

### Translates
If your email template supports translation, to set language add lang parameter.
```
http://app.localhost/mail/auth/password/forgot?lang=ru
```

### Sending mails
If you need to send you email, just add email address in the end of the mail view endpoint.
```
    http://app.localhost/mail/auth/password/forgot/email@gmail.com
```
Logic uses Mail::to(email)->send(Mailable) to send. 

### Development
Want to contribute? Great! Be free to make a pull request!
